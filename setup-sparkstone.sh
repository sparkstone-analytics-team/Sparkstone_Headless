#!/bin/bash

sudo apt-get -y install python-pip python-dev libffi-dev libssl-dev libxml2-dev libjpeg8-dev zlib1g-dev build-essential git libblas-dev liblapack-dev libatlas-base-dev gfortran
